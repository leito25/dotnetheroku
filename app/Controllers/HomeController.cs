﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using app.Models;
using System.IO;

namespace app.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Export()
        {
            ViewData["Message"] = "sim exported success";
            Console.WriteLine("View()");

            //Process.Start("sh", "/Users/innovativeeducation/Documents/LeoIE/LAB/dotNet/dotnetheroku/app/scripttest.sh");
            Process.Start("sh", "scripttest.sh");

            DirectoryInfo info = new DirectoryInfo(".");
            Console.WriteLine("Directory Info:   " + info.FullName); 

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
